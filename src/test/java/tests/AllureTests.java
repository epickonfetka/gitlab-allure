package tests;

import io.qameta.allure.Allure;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AllureTests {
    @Test
    public void failedTest(){
        Assertions.assertTrue(false);
    }

    @Test
    public void notFailedTest(){
        Assertions.assertTrue(true);
    }

    @Test
    public void testWithStep(){
        Allure.step("Калькулятор 2+2", () -> {
            int a = 2;
            int b = 2;
            Assertions.assertEquals(4, a+b);
        });
    }
}
